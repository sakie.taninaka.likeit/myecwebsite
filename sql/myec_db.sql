CREATE DATABASE myec_db DEFAULT CHARACTER SET utf8;

USE myec_db;

CREATE TABLE m_delivery_method (
delivery_method_id int(11) PRIMARY KEY AUTO_INCREMENT,
delivery_price int(11) NOT NULL
) 

ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO m_delivery_method (delivery_method_id, delivery_price) VALUES
(1, 500),
(2, 1000);


CREATE TABLE m_category (
  category_id int(11) PRIMARY KEY AUTO_INCREMENT,
  category_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO m_category (category_id, category_name) VALUES
(1,'椅子'),
(2,'ソファ'),
(3,'テーブル・デスク'),
(4,'ベッド'),
(5,'収納'),
(6,'ミラー'),
(7,'雑貨');


CREATE TABLE m_item (
  item_id int(11) PRIMARY KEY AUTO_INCREMENT,
  item_category int(11) DEFAULT NULL,
  item_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  item_detail text COLLATE utf8_unicode_ci,
  item_price int(11) DEFAULT NULL,
  item_file_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  create_date datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO m_item (item_id,item_category, item_name, item_detail, item_price, item_file_name, create_date) VALUES
(1,1, 'オレンジのスツール', 'オレンジ色のスツール', 6880, '1 (8).jpg', NOW());


CREATE TABLE t_buy (
  buy_id int(11) PRIMARY KEY AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  total_price int(11) NOT NULL,
  delivery_method_id int(11) NOT NULL,
  create_date datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE t_buy_detail (
  buy_detail_id int(11) PRIMARY KEY AUTO_INCREMENT,
  buy_id int(11) NOT NULL,
  item_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE t_user (
  user_id int(11) PRIMARY KEY AUTO_INCREMENT,
  user_name varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_address varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_mail varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_phone_number varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  user_password varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  create_date date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE t_favorite (
  favorite_id int(11) PRIMARY KEY AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  item_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;














DROP TABLE m_item;

DROP DATABASE myec_db;
