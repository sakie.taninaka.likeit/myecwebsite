package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import base.DBManager;
import beans.UserDataBeans;
import myec.MyEcHelper;

public class UserDAO {
	/**
	 * ユーザーのメールアドレスを取得
	 *
	 */
	public static String getUserMail(String inputMail, String inputPassword) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE user_mail = ?");
			st.setString(1, inputMail);

			ResultSet rs = st.executeQuery();

			String userMail = null;
			while (rs.next()) {
				if (MyEcHelper.getSha256(inputPassword).equals(rs.getString("user_password"))) {
					userMail = rs.getString("user_mail");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userMail by inputUserMail has been completed");
			return userMail;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * userMailの重複チェック
	 */
	public static boolean isOverlapLoginId(String inputUserMail, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたuser_idが存在するか調べる
			st = con.prepareStatement("SELECT user_mail FROM t_user WHERE user_mail = ? AND user_id != ?");
			st.setString(1, inputUserMail);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	/**
	 * 新規会員登録の情報を挿入
	 */
	public static void insertUser(UserDataBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user (user_name,user_address,user_mail,user_phone_number,user_password,create_date) VALUES(?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getAddress());
			st.setString(3, udb.getMail());
			st.setString(4, udb.getPhone());
			st.setString(5, MyEcHelper.getSha256(udb.getPassword()));
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * userMailからuserIdを取得
	 *
	 */
	public static int getUserIdByUserMail(String inputMail) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE user_mail = ?");
			st.setString(1, inputMail);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
					userId = rs.getInt("user_id");

			}

			System.out.println("searching userMail by inputUserMail has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * userMailから会員情報を取得
	 *
	 */
	public static UserDataBeans getUserDetailByUserMail(String inputMail) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		UserDataBeans udb = new UserDataBeans();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_user WHERE user_mail = ? ");
			st.setString(1, inputMail);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				udb.setName(rs.getString("user_name"));
				udb.setAddress(rs.getString("user_address"));
				udb.setMail(rs.getString("user_mail"));
				udb.setPhone(rs.getString("user_phone_number"));
			}

			st.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return udb;
	}
	/**
	 * 会員情報の更新
	 */
	public static void updateUser(UserDataBeans udb, int userId) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET user_name = ?, user_address = ?, user_mail = ?, user_phone_number = ?, user_password = ? WHERE user_id = ? ");
			st.setString(1, udb.getName());
			st.setString(2, udb.getAddress());
			st.setString(3, udb.getMail());
			st.setString(4, udb.getPhone());
			st.setString(5, MyEcHelper.getSha256(udb.getPassword()));
			st.setInt(6, userId);
			st.executeUpdate();
			System.out.println("update has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}