package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;

public class CategoryDAO {
	/**
	 * 商品カテゴリの取得
	 */
	public static String getCategoryByID(int categoryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_category WHERE category_id = ?");
			st.setInt(1, categoryId);

			ResultSet rs = st.executeQuery();
			String categoryName = null;
			while (rs.next()) {
				categoryName = rs.getString("category_name");
			}

			return categoryName;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
