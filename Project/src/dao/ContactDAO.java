package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.ContactDataBeans;

public class ContactDAO {

	/**
	 * お問い合わせ内容を挿入
	 */
	public static void insertContactDetail(ContactDataBeans cdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_contact (contact_name,contact_mail,contact_phone,contact_detail,create_date) VALUES(?,?,?,?,?)");
			st.setString(1, cdb.getName());
			st.setString(2, cdb.getMail());
			st.setString(3, cdb.getPhone());
			st.setString(4, cdb.getMessage());
			st.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting contact detail has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * お問い合わせIDによる内容検索
	 */
	public static ContactDataBeans getContactDataBeansByContactId(int contactId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_contact WHERE contact_id = ?");
			st.setInt(1, contactId);

			ResultSet rs = st.executeQuery();

			ContactDataBeans cdb = new ContactDataBeans();
			if (rs.next()) {
				cdb.setId(rs.getInt("contact_id"));
				cdb.setName(rs.getString("contact_name"));
				cdb.setMail(rs.getString("contact_mail"));
				cdb.setPhone(rs.getString("contact_phone"));
				cdb.setMessage(rs.getString("contact_detail"));
			}

			System.out.println("searching ContactDataBeans by contactID has been completed");

			return cdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	 /**
     * お問い合わせ内容一覧
     */
	public static ArrayList<ContactDataBeans> findAllContactData() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ContactDataBeans> contactDataList = new ArrayList<ContactDataBeans>();

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_contact");

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ContactDataBeans cdb = new ContactDataBeans();
				cdb.setId(rs.getInt("contact_id"));
				cdb.setName(rs.getString("contact_name"));
				cdb.setMail(rs.getString("contact_mail"));
				cdb.setPhone(rs.getString("contact_phone"));
				cdb.setMessage(rs.getString("contact_detail"));
				cdb.setContactDate(rs.getTimestamp("create_date"));

				contactDataList.add(cdb);
			}

			System.out.println("searching ContactDataList has been completed");
			return contactDataList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



}

