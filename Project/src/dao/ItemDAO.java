package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.FavoriteItemDataBeans;
import beans.ItemDataBeans;

/**
*
*アイテムに関するDAO
*/
public class ItemDAO {

	/**
	 * 指定カテゴリから5つ分ItemDataBeansを取得
	 */
	public static ArrayList<ItemDataBeans> getItemByCategory(int category) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? ORDER BY create_date DESC LIMIT 5 ");
			st.setInt(1, category);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 */
	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE item_id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("item_id"));
				item.setName(rs.getString("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 */
	public static ArrayList<ItemDataBeans> getItemsByItemName1(String keyWord, int category, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (keyWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? ORDER BY item_price DESC LIMIT ?,? ");
				st.setInt(1, category);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? AND item_name LIKE ? ORDER BY item_price DESC LIMIT ?,? ");
				st.setInt(1, category);
				st.setString(2,"%"+ keyWord +"%");
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<ItemDataBeans> getItemsByItemName2(String keyWord, int category, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (keyWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? ORDER BY item_price ASC LIMIT ?,? ");
				st.setInt(1, category);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? AND item_name LIKE ?  ORDER BY item_price ASC LIMIT ?,? ");
				st.setInt(1, category);
				st.setString(2,"%"+ keyWord +"%");
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	public static ArrayList<ItemDataBeans> getItemsByItemName3(String keyWord, int category, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (keyWord.length() == 0) {
				// 全検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? ORDER BY create_date DESC LIMIT ?,? ");
				st.setInt(1, category);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else {
				// 商品名検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? AND item_name LIKE ?  ORDER BY create_date DESC LIMIT ?,? ");
				st.setInt(1, category);
				st.setString(2,"%"+ keyWord +"%");
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/**
	 * カテゴリの全商品を取得
	 */
	public static ArrayList<ItemDataBeans> getItemsByCategory(int categoryId, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE item_category = ? ORDER BY create_date DESC LIMIT ?,? ");
			st.setInt(1, categoryId);
			st.setInt(2, startiItemNum);
			st.setInt(3, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by category has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	/**
	 * 商品総数を取得
	 */
	public static double getItemCount(String keyWord, int category) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where item_name like ? AND item_category = ? ");
			st.setString(1, "%" + keyWord + "%");
			st.setInt(2, category);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
	 * お気に入りリスト商品総数を取得
	 */
	public static double getFavoriteItemCount(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from t_favorite WHERE user_id = ? ");
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 新商品の情報を挿入
	 */
	public static void insertItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO m_item (item_category,item_name,item_detail,item_price,item_file_name,create_date) VALUES(?,?,?,?,?,?)");
			st.setInt(1, idb.getCategory());
			st.setString(2, idb.getName());
			st.setString(3, idb.getDetail());
			st.setInt(4, idb.getPrice());
			st.setString(5, idb.getFileName());
			st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			st.executeUpdate();
			System.out.println("inserting item has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品を削除する
	 */
	public static void deleteItem(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM m_item WHERE item_id = ? ");
			st.setInt(1, itemId);
			st.executeUpdate();
			System.out.println("delete item has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品を更新する
	 */
	public static void updateItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE m_item SET item_name = ?, item_detail = ?, item_price = ? WHERE item_id = ? ");
			st.setString(1, idb.getName());
			st.setString(2, idb.getDetail());
			st.setInt(3, idb.getPrice());
			st.setInt(4, idb.getId());
			st.executeUpdate();
			System.out.println("update item has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * お気に入りリストから商品を削除する
	 */
	public static void deleteFavoriteItem(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM t_favorite WHERE item_id = ? ");
			st.setInt(1, itemId);
			st.executeUpdate();
			System.out.println("delete favorite item has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}





	/**
	 * カテゴリの商品総数を取得
	 */
	public static double getItemCountByCategory(int categoryId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where item_category = ?");
			st.setInt(1, categoryId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * userMailの重複チェック
	 */
	public static boolean isOverlapFavoriteItem(int itemId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// お気に入りリストに該当商品が存在するか調べる
			st = con.prepareStatement("SELECT * FROM t_favorite WHERE user_id = ? AND item_id= ? ");
			st.setInt(1, userId);
			st.setInt(2, itemId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching favorite item by itemID & userID has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}


	/**
	 * お気に入りリストに商品を追加
	 */
	public static void insertFavoriteItem(FavoriteItemDataBeans fidb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_favorite (user_id, item_id) VALUES(?,?)");
			st.setInt(1, fidb.getUserId());
			st.setInt(2, fidb.getItemId());

			st.executeUpdate();
			System.out.println("inserting favorite has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * お気に入りリストを登録メールアドレスから取得
	 */
	public static ArrayList<ItemDataBeans> getFavoriteItemsByUserMail(int userId, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			// 商品名検索
			st = con.prepareStatement("SELECT * FROM m_item INNER JOIN t_favorite ON m_item.item_id = t_favorite.item_id  WHERE user_id = ? ORDER BY favorite_id ASC LIMIT ?,? ");
			st.setInt(1, userId);
			st.setInt(2, startiItemNum);
			st.setInt(3, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> favoriteItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				favoriteItemList.add(item);
			}
			System.out.println("get FavoriteItems by userID has been completed");
			return favoriteItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * お気に入りリストを登録メールアドレスから取得(TOPページ用)
	 */
	public static ArrayList<ItemDataBeans> getFavoriteItemsByUserMail2(int userId, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			// 商品名検索
			st = con.prepareStatement("SELECT * FROM m_item INNER JOIN t_favorite ON m_item.item_id = t_favorite.item_id  WHERE user_id = ? ORDER BY favorite_id ASC LIMIT ? ");
			st.setInt(1, userId);
			st.setInt(2, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> favoriteItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				favoriteItemList.add(item);
			}
			System.out.println("get FavoriteItems by userID has been completed");
			return favoriteItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

    /**
     * お気に入りリストから商品の削除
     */
    public static FavoriteItemDataBeans deleteItem(int userId, int itemId) {
        Connection con = null;
        PreparedStatement st = null;

        try {
        	con = DBManager.getConnection();

        	// 商品をリストから削除
        	st = con.prepareStatement("DELETE FROM t_favorite WHERE user_id = ? AND item_id = ? ");
        	st.setInt(1, userId);
        	st.setInt(2, itemId);

            st.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		return null;
    }

	/**
	 * 新着商品を5つ取得(TOPページ用)
	 */
	public static ArrayList<ItemDataBeans> getNewItems(int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			// 商品名検索
			st = con.prepareStatement("SELECT * FROM m_item ORDER BY create_date DESC LIMIT ? ");
			st.setInt(1, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> newItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("item_id"));
				item.setCategory(rs.getInt("item_category"));
				item.setName(rs.getString("item_name"));
				item.setDetail(rs.getString("item_detail"));
				item.setPrice(rs.getInt("item_price"));
				item.setFileName(rs.getString("item_file_name"));
				newItemList.add(item);
			}
			System.out.println("get NewItems by userID has been completed");
			return newItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


}

