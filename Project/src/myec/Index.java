package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * トップページ
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//表示する商品数
	final static int PAGE_MAX_NEW_ITEM_COUNT = 5;
	final static int PAGE_MAX_ITEM_COUNT = 13;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

		String validationMessage2 = (String) MyEcHelper.cutSessionAttribute(session, "validationMessage2");

		try {
			if(isLogin == false) {
				validationMessage2 = "ログイン・新規会員登録をするとお気に入りリスト機能が使用できます";
				session.setAttribute("validationMessage2", validationMessage2);

				// 新着商品を取得
				ArrayList<ItemDataBeans> newItemList = ItemDAO.getNewItems(PAGE_MAX_NEW_ITEM_COUNT);
				request.setAttribute("newItemList", newItemList);

				request.getRequestDispatcher(MyEcHelper.INDEX_PAGE).forward(request, response);

			}else {
				//登録メールアドレスからユーザーIDを取得
				String inputUserMail = (String) session.getAttribute("inputMail");
				int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

				// お気に入りリストを取得
				ArrayList<ItemDataBeans> favoriteItemList = ItemDAO.getFavoriteItemsByUserMail2(userId, PAGE_MAX_ITEM_COUNT);
				request.setAttribute("favoriteItemList", favoriteItemList);

				// 新着商品を取得
				ArrayList<ItemDataBeans> newItemList = ItemDAO.getNewItems(PAGE_MAX_NEW_ITEM_COUNT);
				request.setAttribute("newItemList", newItemList);

				request.getRequestDispatcher(MyEcHelper.INDEX_PAGE).forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
