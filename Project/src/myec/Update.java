package myec;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * 会員情報更新ページ
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String validationMessage = (String) MyEcHelper.cutSessionAttribute(session, "validationMessage");
		request.setAttribute("validationMessage",validationMessage);

		try {
			//ログイン中の登録メールアドレスを取得
			String inputUserMail = (String) session.getAttribute("inputMail");

			//登録メールアドレスから会員情報を取得
			UserDataBeans udb = new UserDataBeans();
			udb = UserDAO.getUserDetailByUserMail(inputUserMail);

			session.setAttribute("udb", udb);



		} catch (SQLException e) {
			e.printStackTrace();
		}





		request.getRequestDispatcher(MyEcHelper.UPDATE_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
