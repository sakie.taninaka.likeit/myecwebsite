package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class EditResult
 */
@WebServlet("/EditResult")
public class EditResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(MyEcHelper.EDIT_RESULT_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String inputItemName = request.getParameter("item-name");
			String inputItemPrice = request.getParameter("item-price");
			String inputItemDetail = request.getParameter("item-detail");

			int itemPrice = Integer.parseInt(inputItemPrice);

			ItemDataBeans idb = new ItemDataBeans();
			idb.setName(inputItemName);
			idb.setPrice(itemPrice);
			idb.setDetail(inputItemDetail);
			String validationMessage = "";

			// 空欄がないかどうか
			if (inputItemName==null) {
				validationMessage += "商品名を入力してください<br>";
			}

			if (itemPrice == 0) {
				validationMessage += "価格を入力してください<br>";
			}



			// バリデーションエラーメッセージがないなら商品情報更新
			if (validationMessage.length() == 0) {
				session.setAttribute("idb", idb);
				try {
					ItemDAO.updateItem(idb);
					} catch (Exception e) {
						e.printStackTrace();
					}
				request.getRequestDispatcher(MyEcHelper.EDIT_RESULT_PAGE).forward(request, response);
			} else {
				session.setAttribute("idb", idb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("Edit");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}