package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;

/**
 * ログイン情報の確認
 */
@WebServlet("/LoginResult")
public class LoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {

			/**ログインボタン**/
			// リクエストパラメータの取得（ログイン情報）
	        String inputMail = request.getParameter("mail");
	        String inputPassword = request.getParameter("pw");
	        // セッションスコープに保存
	        session.setAttribute("inputMail", inputMail);
	        session.setAttribute("inputPassword", inputPassword);

	      //登録メールアドレスユーザーIDを取得
			String userMail = UserDAO.getUserMail(inputMail, inputPassword);
			String inputUserMail = (String) session.getAttribute("inputMail");
			int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

			//メールアドレスを取得が取得できたなら
			if (userMail != null) {
				session.setAttribute("isLogin", true);
				session.setAttribute("mail", inputMail);
				session.setAttribute("userId", userId);

				if (userId==1) {
					session.setAttribute("isAdmin", true);
					request.getRequestDispatcher(MyEcHelper.ADMIN_PAGE).forward(request, response);
				} else {
				//マイページに遷移
				request.getRequestDispatcher(MyEcHelper.MY_PAGE).forward(request, response);
				}
			//メールアドレスが取得できなかったら
			} else {
				session.setAttribute("mail", inputMail);
				session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
				response.sendRedirect("Login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
