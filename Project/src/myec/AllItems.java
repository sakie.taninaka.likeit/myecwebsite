package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.CategoryDAO;
import dao.ItemDAO;

/**
 * ITEMSページ
 */
@WebServlet("/AllItems")
public class AllItems extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//1ページに表示する商品数
		final static int PAGE_MAX_ITEM_COUNT = 18;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//選択されたカテゴリのIDを型変換し利用
			String id = request.getParameter("category");
			int categoryId = Integer.parseInt(id);

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			//対象のアイテム情報を取得
			ArrayList<ItemDataBeans> itemList = ItemDAO.getItemsByCategory(categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
			String categoryName = CategoryDAO.getCategoryByID(categoryId);

			//セッションに格納
			session.setAttribute("itemList", itemList);
			session.setAttribute("category", id);
			session.setAttribute("categoryId", categoryId);
			session.setAttribute("categoryName", categoryName);

			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDAO.getItemCountByCategory(categoryId);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", itemList);

			request.getRequestDispatcher(MyEcHelper.ALL_ITEMS_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
