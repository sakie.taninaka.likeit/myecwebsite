package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyHistoryBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * Servlet implementation class BuyHistory
 */
@WebServlet("/BuyHistory")
public class BuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//ログイン中の登録メールアドレスからユーザーIDを取得
			String inputUserMail = (String) session.getAttribute("inputMail");
			int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserメールでユーザーを取得
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDetailByUserMail(inputUserMail) : (UserDataBeans) MyEcHelper.cutSessionAttribute(session, "returnUDB");


			//Daoで購入情報取得
			BuyDAO buyDAO = new BuyDAO();
			ArrayList<BuyHistoryBeans> buyHistoryList = buyDAO.getBuyHistoryBeansByUserId(userId);

			//リクエストスコープにセット
			request.setAttribute("buyHistoryList", buyHistoryList);

			request.setAttribute("udb", udb);

			request.getRequestDispatcher(MyEcHelper.BUY_HISTORY_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
