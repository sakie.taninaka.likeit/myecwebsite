package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * ログアウト画面
 */
@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Boolean isLogin = (Boolean) MyEcHelper.cutSessionAttribute(session, "isLogin");
		Boolean isAdmin = (Boolean) MyEcHelper.cutSessionAttribute(session, "isAdmin");
		String inputUserMail = (String) MyEcHelper.cutSessionAttribute(session, "inputMail");
		session.removeAttribute("userId");
		request.getRequestDispatcher(MyEcHelper.LOGOUT_PAGE).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
