package myec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoriteItemDataBeans;
import beans.ItemDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * お気に入りリスト画面
 */
@WebServlet("/Favorite")
public class Favorite extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//1ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 18;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
		String validationMessage = (String) MyEcHelper.cutSessionAttribute(session, "validationMessage");
		request.setAttribute("validationMessage",validationMessage);
		String validationMessage2 = (String) MyEcHelper.cutSessionAttribute(session, "validationMessage2");

		try {
			if(isLogin == false) {
				validationMessage2 = "ログイン・新規会員登録をするとお気に入りリスト機能が使用できます";
				session.setAttribute("validationMessage2", validationMessage2);
				request.getRequestDispatcher(MyEcHelper.FAVORITE_PAGE).forward(request, response);
			}else {
			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			//登録メールアドレスからユーザーIDを取得
			String inputUserMail = (String) session.getAttribute("inputMail");
			int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

			// お気に入りリストを取得 ページ表示分のみ
			ArrayList<ItemDataBeans> favoriteItemList = ItemDAO.getFavoriteItemsByUserMail(userId, pageNum, PAGE_MAX_ITEM_COUNT);

			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDAO.getFavoriteItemCount(userId);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);
			// 総ページ数
			request.setAttribute("pageMax", pageMax);
			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", favoriteItemList);

			request.getRequestDispatcher(MyEcHelper.FAVORITE_PAGE).forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		//パラメータの取得
		String favoriteItemId = request.getParameter("delete_item_id");
		int itemId = Integer.parseInt(favoriteItemId);

		String inputUserMail = (String) session.getAttribute("inputMail");
		int userId;

		try {
			userId = UserDAO.getUserIdByUserMail(inputUserMail);
			// idを引数に渡してDaoでDELETEを実行
			FavoriteItemDataBeans fidb = ItemDAO.deleteItem(userId, itemId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// お気に入りリストページにリダイレクト
		String validationMessage = "お気に入りリストから商品を削除しました";
		session.setAttribute("validationMessage", validationMessage);
		response.sendRedirect("Favorite");
	}

}
