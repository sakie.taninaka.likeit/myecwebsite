package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/BuyHistoryDetail")
public class BuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			//ログイン中の登録メールアドレスからユーザーIDを取得
			String inputUserMail = (String) session.getAttribute("inputMail");
			int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserメールでユーザーを取得
			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDetailByUserMail(inputUserMail) : (UserDataBeans) MyEcHelper.cutSessionAttribute(session, "returnUDB");

			// URLからGETパラメータとして購入IDを受け取る
			String id = request.getParameter("buy_id");
			int buyId = Integer.parseInt(id);

			// 確認用：購入idをコンソールに出力
			System.out.println(buyId);

			// 購入idから情報を取得
			BuyDAO buyDAO = new BuyDAO();
			BuyDataBeans BDB =  buyDAO.getBuyDataBeansByBuyId(buyId);
			BDB.setSubTotalPrice(BDB.getTotalPrice()-BDB.getDeliveryMethodPrice());

			// 情報をリクエストスコープにセット
			request.setAttribute("BDB", BDB);

			// 購入idから商品情報を取得
			BuyDetailDAO buyDetailDAO = new BuyDetailDAO();
			ArrayList<ItemDataBeans> buyDetailItemList = buyDetailDAO.getItemDataBeansListByBuyId(buyId);

			// 情報をリクエストスコープにセット
			request.setAttribute("buyDetailItemList", buyDetailItemList);
			request.setAttribute("udb", udb);

			request.getRequestDispatcher(MyEcHelper.BUY_HISTORY_DETAIL_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
