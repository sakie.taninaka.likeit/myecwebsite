package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * 検索結果ページ
 */
@WebServlet("/EditSerchResult")
public class EditSerchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//1ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 18;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {
			String keyWord = request.getParameter("keyword");
			String inputCategory = request.getParameter("category");
			String inputSort = request.getParameter("sort");

			int category = Integer.parseInt(inputCategory);
			int sort = Integer.parseInt(inputSort);

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("keyWord", keyWord);
			session.setAttribute("category", category);
			session.setAttribute("sort", sort);


			// 並び替えが価格(高い順)のとき
			if(sort == 1) {
				// 商品リストを取得 ページ表示分のみ
				ArrayList<ItemDataBeans> searchResultItemList = ItemDAO.getItemsByItemName1(keyWord, category, pageNum, PAGE_MAX_ITEM_COUNT);

				// 検索ワードに対しての総ページ数を取得
				double itemCount = ItemDAO.getItemCount(keyWord, category);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("itemList", searchResultItemList);

				request.getRequestDispatcher(MyEcHelper.EDIT_SERCH_RESULT_PAGE).forward(request, response);
			}
			if(sort == 2) {
				// 商品リストを取得 ページ表示分のみ
				ArrayList<ItemDataBeans> searchResultItemList = ItemDAO.getItemsByItemName2(keyWord, category, pageNum, PAGE_MAX_ITEM_COUNT);

				// 検索ワードに対しての総ページ数を取得
				double itemCount = ItemDAO.getItemCount(keyWord, category);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("itemList", searchResultItemList);

				request.getRequestDispatcher(MyEcHelper.EDIT_SERCH_RESULT_PAGE).forward(request, response);
			}
			if(sort == 3) {
				// 商品リストを取得 ページ表示分のみ
				ArrayList<ItemDataBeans> searchResultItemList = ItemDAO.getItemsByItemName3(keyWord, category, pageNum, PAGE_MAX_ITEM_COUNT);

				// 検索ワードに対しての総ページ数を取得
				double itemCount = ItemDAO.getItemCount(keyWord, category);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
				request.setAttribute("itemList", searchResultItemList);

				request.getRequestDispatcher(MyEcHelper.EDIT_SERCH_RESULT_PAGE).forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
