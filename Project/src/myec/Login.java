package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * ログイン画面
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		//ログイン失敗時に使用するため
		String inputMail =session.getAttribute("user_mail")!=null?(String) MyEcHelper.cutSessionAttribute(session,"user_mail"):"";
		String loginErrorMessage = (String)MyEcHelper.cutSessionAttribute(session, "loginErrorMessage");

		request.setAttribute("inputMail", inputMail);
		request.setAttribute("loginErrorMessage", loginErrorMessage);

		request.getRequestDispatcher(MyEcHelper.LOGIN_PAGE).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
