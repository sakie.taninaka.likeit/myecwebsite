package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import dao.DeliveryMethodDAO;
import dao.UserDAO;

/**
 * 購入確認画面
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//買い物かご
			ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
			//小計
			int subTotalPrice = MyEcHelper.getTotalItemPrice(cartIDBList);
			//ログイン中の登録メールアドレスを取得
			String inputUserMail = (String) session.getAttribute("inputMail");

			//登録メールアドレスから会員情報を取得
			int userId = UserDAO.getUserIdByUserMail(inputUserMail);

			//購入金額10万円以上の時は送料500円
			if (subTotalPrice >= 100000) {
				DeliveryMethodDataBeans DMB = DeliveryMethodDAO.getDeliveryMethodDataBeansByID(1);

				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setUserId(userId);
				bdb.setSubTotalPrice(subTotalPrice);
				bdb.setTotalPrice(subTotalPrice+DMB.getPrice());
				bdb.setDelivertMethodId(DMB.getId());
				bdb.setDeliveryMethodPrice((int) DMB.getPrice());
				bdb.getFormatTotalPrice();
				bdb.getFormatSubTotalPrice();
				//購入確定で利用
				session.setAttribute("bdb", bdb);
				request.getRequestDispatcher(MyEcHelper.BUY_CONFIRM_PAGE).forward(request, response);
			}
			//購入金額10万円未満の時は送料1000円
			if (subTotalPrice < 100000) {
				DeliveryMethodDataBeans DMB = DeliveryMethodDAO.getDeliveryMethodDataBeansByID(2);

				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setUserId(userId);
				bdb.setSubTotalPrice(subTotalPrice);
				bdb.setTotalPrice(subTotalPrice+DMB.getPrice());
				bdb.setDelivertMethodId(DMB.getId());
				bdb.setDeliveryMethodPrice((int) DMB.getPrice());
				bdb.getFormatTotalPrice();
				bdb.getFormatSubTotalPrice();
				bdb.getFormatDeliveryMethodPrice();
				//購入確定で利用
				session.setAttribute("bdb", bdb);
				request.getRequestDispatcher(MyEcHelper.BUY_CONFIRM_PAGE).forward(request, response);
			}


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
