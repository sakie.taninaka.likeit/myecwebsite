package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *　MyPage画面
 */
@WebServlet("/MyPage")
public class MyPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
		 Boolean isAdmin = session.getAttribute("isAdmin")!=null?(Boolean) session.getAttribute("isAdmin"):false;

		if(isLogin == false) {
			response.sendRedirect("Login");
		}else {
			if (isAdmin == true) {
				request.getRequestDispatcher(MyEcHelper.ADMIN_PAGE).forward(request, response);
			} else {
			//マイページに遷移
				String userMail = (String) session.getAttribute("inputMail");
				request.getRequestDispatcher(MyEcHelper.MY_PAGE).forward(request, response);
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
