package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * 会員情報更新確認
 */
@WebServlet("/UpdateConfirm")
public class UpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	/**
	* 入力内容を確認 バリデーションエラーがある場合更新画面に遷移
	*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String updateUserName = request.getParameter("name");
			String updateUserMail = request.getParameter("mail");
			String updateConfirmUserMail = request.getParameter("mail2");
			String updateUserPhone = request.getParameter("phone");
			String updateUserAddress = request.getParameter("address");
			String updatePassword = request.getParameter("pw");
			String updateConfirmPassword = request.getParameter("pw2");

			//ログイン中の登録メールアドレスからユーザーIDを取得
			String inputUserMail = (String) session.getAttribute("inputMail");
			int userId =  UserDAO.getUserIdByUserMail(inputUserMail);

			UserDataBeans udb = new UserDataBeans();
			udb.setName(updateUserName);
			udb.setMail(updateUserMail);
			udb.setPhone(updateUserPhone);
			udb.setAddress(updateUserAddress);
			udb.setPassword(updatePassword);


			String validationMessage = "";

			// 入力されているパスワードが確認用と等しいか
			if (!updatePassword.equals(updateConfirmPassword)) {
				validationMessage += "入力されたパスワードと確認用パスワードが違います<br>";
			}

			// 入力されているメールアドレスが確認用と等しいか
			if (!updateUserMail.equals(updateConfirmUserMail)) {
				validationMessage += "入力されたメールアドレスと確認用メールアドレスが違います<br>";
			}

			// 登録メールアドレスの重複をチェック
			if (UserDAO.isOverlapLoginId(udb.getMail(), userId)) {
				validationMessage += "入力されたメールアドレスはすでに登録されています";
			}


			// バリデーションエラーメッセージがないなら会員情報更新
			if (validationMessage.length() == 0) {
				session.setAttribute("udb", udb);
				session.setAttribute("returnUDB", udb);
				try {
					UserDAO.updateUser(udb, userId);
					} catch (Exception e) {
						e.printStackTrace();
					}
				request.getRequestDispatcher(MyEcHelper.UPDATE_RESULT_PAGE).forward(request, response);
			} else {
				session.setAttribute("udb", udb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("Update");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
