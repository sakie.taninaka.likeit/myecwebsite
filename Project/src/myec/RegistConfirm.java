package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;


/**
 * 新規会員登録情報確認
 */
@WebServlet("/RegistConfirm")
public class RegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	/**
	* 入力内容を確認 バリデーションエラーがある場合新規登録画面に遷移
	*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String inputUserName = request.getParameter("name");
			String inputUserMail = request.getParameter("mail");
			String inputConfirmUserMail = request.getParameter("mail2");
			String inputUserPhone = request.getParameter("phone");
			String inputUserAddress = request.getParameter("address");
			String inputPassword = request.getParameter("pw");
			String inputConfirmPassword = request.getParameter("pw2");

			UserDataBeans udb = new UserDataBeans();
			udb.setName(inputUserName);
			udb.setMail(inputUserMail);
			udb.setPhone(inputUserPhone);
			udb.setAddress(inputUserAddress);
			udb.setPassword(inputPassword);

			String validationMessage = "";

			// 入力されているパスワードが確認用と等しいか
			if (!inputPassword.equals(inputConfirmPassword)) {
					validationMessage += "入力されたパスワードと確認用パスワードが違います<br>";
			}

			// 入力されているメールアドレスが確認用と等しいか
			if (!inputUserMail.equals(inputConfirmUserMail)) {
					validationMessage += "入力されたメールアドレスと確認用メールアドレスが違います<br>";
			}

			// 登録メールアドレスの重複をチェック
			if (UserDAO.isOverlapLoginId(udb.getMail(), 0)) {
				validationMessage += "入力されたメールアドレスはすでに登録されています";
			}



			// バリデーションエラーメッセージがないなら会員情報登録
			if (validationMessage.length() == 0) {
				session.setAttribute("udb", udb);
				try {
					UserDAO.insertUser(udb);
					} catch (Exception e) {
						e.printStackTrace();
					}
				session.setAttribute("isLogin", true);
				session.setAttribute("inputMail", inputUserMail);
				request.getRequestDispatcher(MyEcHelper.REGIST_RESULT_PAGE).forward(request, response);
			} else {
				session.setAttribute("udb", udb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("Regist");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
