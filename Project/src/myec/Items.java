package myec;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * ITEMSページ
 */
@WebServlet("/Items")
public class Items extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList1 = ItemDAO.getItemByCategory(1);
			//リクエストスコープにセット
			request.setAttribute("itemList1", itemList1);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList2 = ItemDAO.getItemByCategory(2);
			//リクエストスコープにセット
			request.setAttribute("itemList2", itemList2);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList3 = ItemDAO.getItemByCategory(3);
			//リクエストスコープにセット
			request.setAttribute("itemList3", itemList3);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList4 = ItemDAO.getItemByCategory(4);
			//リクエストスコープにセット
			request.setAttribute("itemList4", itemList4);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList5 = ItemDAO.getItemByCategory(5);
			//リクエストスコープにセット
			request.setAttribute("itemList5", itemList5);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList6 = ItemDAO.getItemByCategory(6);
			//リクエストスコープにセット
			request.setAttribute("itemList6", itemList6);

			//カテゴリごとの商品情報を取得
			ArrayList<ItemDataBeans>itemList7 = ItemDAO.getItemByCategory(7);
			//リクエストスコープにセット
			request.setAttribute("itemList7", itemList7);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.getRequestDispatcher(MyEcHelper.ITEMS_PAGE).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
