package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoriteItemDataBeans;
import dao.ItemDAO;
import dao.UserDAO;

/**
 * Servlet implementation class FavoriteAdd
 */
@WebServlet("/FavoriteAdd")
public class FavoriteAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;

		String validationMessage = "";

		try {
			if(isLogin == false) {
				validationMessage = "ログイン・新規会員登録をするとお気に入りリスト機能が使用できます";
				session.setAttribute("validationMessage2", validationMessage);
				request.getRequestDispatcher(MyEcHelper.FAVORITE_PAGE).forward(request, response);

			}else {
				//パラメータ・セッションスコープの取得
				String favoriteItemId = request.getParameter("item_id");
				int itemId = Integer.parseInt(favoriteItemId);
				String inputUserMail = (String) session.getAttribute("inputMail");
				//取得したログインメールアドレスからユーザーidを取得
				int userId = UserDAO.getUserIdByUserMail(inputUserMail);

				FavoriteItemDataBeans fidb = new FavoriteItemDataBeans();
				fidb.setItemId(itemId);
				fidb.setUserId(userId);

				session.setAttribute("itemId", itemId);

				boolean isOverlap = ItemDAO.isOverlapFavoriteItem(fidb.getItemId(), fidb.getUserId());


				if(isOverlap == true){
					validationMessage += "すでにお気に入りリストに登録されています";
					session.setAttribute("validationMessage", validationMessage);
					response.sendRedirect("Favorite");
					return;
				}

				if (isOverlap == false) {
					session.setAttribute("fidb", fidb);
					try {
						ItemDAO.insertFavoriteItem(fidb);
						} catch (Exception e) {
							e.printStackTrace();
						}
					validationMessage += "お気に入りリストに登録しました";
					session.setAttribute("validationMessage", validationMessage);
					response.sendRedirect("Favorite");
					return;
				} else {
					session.setAttribute("fidb", fidb);
					session.setAttribute("validationMessage", validationMessage);
					response.sendRedirect("Favorite");
					return;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}
}


