package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ContactDataBeans;
import dao.ContactDAO;

/**
 * Servlet implementation class ContactConfirm
 */
@WebServlet("/ContactConfirm")
public class ContactConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String inputName = request.getParameter("name");
			String inputMail = request.getParameter("mail");
			String inputPhone = request.getParameter("phone");
			String inputMessage = request.getParameter("message");

			ContactDataBeans cdb = new ContactDataBeans();
			cdb.setName(inputName);
			cdb.setMail(inputMail);
			cdb.setPhone(inputPhone);
			cdb.setMessage(inputMessage);
			String validationMessage = "";

			// 空欄がないかどうか
			if (inputName == null) {
				validationMessage += "お名前を入力してください<br>";
			}

			if (inputMail == null) {
				validationMessage += "メールアドレスを入力してください<br>";
			}

			if (inputPhone == null) {
				validationMessage += "電話番号を入力してください<br>";
			}

			if (inputMessage == null) {
				validationMessage += "お問い合わせ内容を入力してください<br>";
			}



			// バリデーションエラーメッセージがないならお問い合わせ内容登録
			if (validationMessage.length() == 0) {
				session.setAttribute("cdb", cdb);
				try {
					ContactDAO.insertContactDetail(cdb);
					} catch (Exception e) {
						e.printStackTrace();
					}
				request.getRequestDispatcher(MyEcHelper.CONTACT_DONE_PAGE).forward(request, response);
			} else {
				session.setAttribute("cdb", cdb);
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("Contact");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");

		}
	}
}
