package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ContactDataBeans;
import dao.ContactDAO;

/**
 * Servlet implementation class ContactDetail
 */
@WebServlet("/ContactDetail")
public class ContactDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// URLからGETパラメータとしてお問い合わせIDを受け取る
			String id = request.getParameter("contact_id");
			int contactId = Integer.parseInt(id);

			// 確認用：お問い合わせidをコンソールに出力
			System.out.println(contactId);

			// お問い合わせidから情報を取得
			ContactDAO contactDAO = new ContactDAO();
			ContactDataBeans CDB =  ContactDAO.getContactDataBeansByContactId(contactId);

			// 情報をリクエストスコープにセット
			request.setAttribute("CDB", CDB);


			request.getRequestDispatcher(MyEcHelper.CONTACT_DETAIL_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
