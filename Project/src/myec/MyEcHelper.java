package myec;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

public class MyEcHelper {
		// about
		static final String ABOUT_PAGE = "/WEB-INF/jsp/about.jsp";
		// access
		static final String ACCESS_PAGE = "/WEB-INF/jsp/access.jsp";
		// 管理者用マイページ
		static final String ADMIN_PAGE = "/WEB-INF/jsp/adminpage.jsp";
		// カテゴリー別商品
		static final String ALL_ITEMS_PAGE = "/WEB-INF/jsp/allitems.jsp";
		// 購入ページ
		static final String BUY_PAGE = "/WEB-INF/jsp/buy.jsp";
		// 購入確認ページ
		static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/buyconfirm.jsp";
		// 購入履歴ページ
		static final String BUY_HISTORY_PAGE = "/WEB-INF/jsp/buyhistory.jsp";
		// 購入履歴詳細ページ
		static final String BUY_HISTORY_DETAIL_PAGE = "/WEB-INF/jsp/buyhistorydetail.jsp";
		// 購入完了ページ
		static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/buyresult.jsp";
		// カートページ
		static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
		// お問い合わせページ
		static final String CONTACT_PAGE = "/WEB-INF/jsp/contact.jsp";
		// お問い合わせ完了ページ
		static final String CONTACT_DONE_PAGE = "/WEB-INF/jsp/contact-done.jsp";
		// お問い合わせ一覧ページ
		static final String CONTACT_LIST_PAGE = "/WEB-INF/jsp/ContactList.jsp";
		// お問い合わせ詳細ページ
		static final String CONTACT_DETAIL_PAGE = "/WEB-INF/jsp/ContactDetail.jsp";
		// 商品編集ページ
		static final String EDIT_PAGE = "/WEB-INF/jsp/edit.jsp";
		// 商品検索編集ページ
		static final String EDIT_SERCH_PAGE = "/WEB-INF/jsp/editserch.jsp";
		// 商品検索結果ページ
		static final String EDIT_SERCH_RESULT_PAGE = "/WEB-INF/jsp/editserchresult.jsp";
		// 商品編集結果ページ
		static final String EDIT_RESULT_PAGE = "/WEB-INF/jsp/editresult.jsp";
		// エラーページ
		static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
		// お気に入りページ
		static final String FAVORITE_PAGE = "/WEB-INF/jsp/favorite.jsp";
		// トップページ
		static final String INDEX_PAGE = "/WEB-INF/jsp/index.jsp";
		// アイテム詳細ページ
		static final String ITEM_PAGE = "/WEB-INF/jsp/itempage.jsp";
		// アイテムカテゴリページ
		static final String ITEMS_PAGE = "/WEB-INF/jsp/items.jsp";
		// ログインページ
		static final String LOGIN_PAGE = "/WEB-INF/jsp/log-in.jsp";
		// ログアウトページ
		static final String LOGOUT_PAGE = "/WEB-INF/jsp/log-out.jsp";
		// マイページ
		static final String MY_PAGE = "/WEB-INF/jsp/my-page.jsp";
		// 商品追加ページ
		static final String PLUS_PAGE = "/WEB-INF/jsp/plus.jsp";
		// 商品追加完了ページ
		static final String PLUS_RESULT_PAGE = "/WEB-INF/jsp/plusresult.jsp";
		// 新規会員登録完了ページ
		static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/register-result.jsp";
		// 新規会員登録ページ
		static final String REGIST_PAGE = "/WEB-INF/jsp/register.jsp";
		// 商品検索ページ
		static final String SERCH_PAGE = "/WEB-INF/jsp/serch.jsp";
		// 検索結果ページ
		static final String SERCH_RESULT_PAGE = "/WEB-INF/jsp/serchresult.jsp";
		// 会員情報更新ページ
		static final String UPDATE_PAGE = "/WEB-INF/jsp/update.jsp";
		// 会員情報更新結果ページ
		static final String UPDATE_RESULT_PAGE = "/WEB-INF/jsp/updateresult.jsp";

		public static MyEcHelper getInstance() {
			return new MyEcHelper();
		}


		/**
		 * ハッシュ関数
		 *
		 */
		public static String getSha256(String target) {
			MessageDigest md = null;
			StringBuffer buf = new StringBuffer();
			try {
				md = MessageDigest.getInstance("SHA-256");
				md.update(target.getBytes());
				byte[] digest = md.digest();

				for (int i = 0; i < digest.length; i++) {
					buf.append(String.format("%02x", digest[i]));
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return buf.toString();
		}

		/**
		 * 商品の合計金額を算出する
		 *
		 */
		public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
			int total = 0;
			for (ItemDataBeans item : items) {
				total += item.getPrice();
			}
			return total;
		}


		/**
		 * セッションから指定データを取得（削除も一緒に行う）
		 *
		 */
		public static Object cutSessionAttribute(HttpSession session, String str) {
			Object test = session.getAttribute(str);
			session.removeAttribute(str);

			return test;
		}


	}
