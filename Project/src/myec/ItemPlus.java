package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * 新アイテム追加画面
 */
@WebServlet("/ItemPlus")
public class ItemPlus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		//入力内容に誤りがある、確認画面で戻るボタン押し下げでアクセスしてきたときはセッションから入力情報を取得
		ItemDataBeans idb = session.getAttribute("idb") != null?(ItemDataBeans) MyEcHelper.cutSessionAttribute(session, "idb"):new ItemDataBeans();
		String validationMessage = (String) MyEcHelper.cutSessionAttribute(session, "validationMessage");

		request.setAttribute("idb", idb);
		request.setAttribute("validationMessage",validationMessage);

		request.getRequestDispatcher(MyEcHelper.PLUS_PAGE).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}