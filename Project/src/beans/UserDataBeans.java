package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable {
	private String name;
	private String address;
	private String mail;
	private String phone;
	private String password;
	private int userId;

	// コンストラクタ
	public UserDataBeans() {
		this.name = "";
		this.address = "";
		this.mail = "";
		this.phone = "";
		this.password = "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}


	/**
	 * ユーザー情報更新時の必要情報をまとめてセットするための処理
	 *
	 */
	public void setUpdateUserDataBeansInfo(String name, String address, String mail, String phone, String password,int id) {
		this.name = name;
		this.address = address;
		this.mail = mail;
		this.phone = phone;
		this.password = password;
	}


}