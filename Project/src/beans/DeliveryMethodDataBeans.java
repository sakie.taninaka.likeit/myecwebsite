package beans;

import java.io.Serializable;

/**
 * 配送方法
 *
 */

public class DeliveryMethodDataBeans implements Serializable {
	private int id;
	private int price;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

}
