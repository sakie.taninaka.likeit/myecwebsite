package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable {
	private int id;
	private int category;
	private String name;
	private String detail;
	private int price;
	private String fileName;



	public int getId() {
		return id;
	}
	public void setId(int itemId) {
		this.id = itemId;
	}
	public String getName() {
		return name;
	}
	public void setName(String itemName) {
		this.name = itemName;
	}

	public int getCategory() {
		return category;
	}
	public void setCategory(int itemCategory) {
		this.category = itemCategory;
	}


	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int inputItemPrice) {
		this.price = inputItemPrice;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String filename) {
		this.fileName = filename;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}



}
