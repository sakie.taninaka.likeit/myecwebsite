<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav>
	<ul class="min-nav">
		<li><a href="MyPage">My Page</a></li>
		<li>/</li>

		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>
		<%if(isLogin){ %>
		<li><a href="Logout">Log Out</a></li>
		<%}else{ %>
		<li><a href="Login">Log In</a></li>
		<%} %>

		<li><a href="Favorite"><i class="far fa-heart"></i></a></li>
		<li><a href="Cart"><i class="fas fa-shopping-cart"></i></a></li>
        <li><a href="Serch"><i class="fas fa-search"></i></a></li>
	</ul>
</nav>

<div class="logo">
	<a href="Index"><img src="img/logo_necoaci.png" alt="necoaci"  width="200px"></a>
</div>


<nav>
	<ul class="global-nav">
		<li><a href="About">ABOUT</a></li>
		<li><a href="Items">ITEMS</a></li>
		<li><a href="Access">ACCESS</a></li>
		<li><a href="Contact">CONTACT</a></li>
        <li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
	</ul>
</nav>