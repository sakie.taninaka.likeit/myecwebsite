<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>商品検索 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/serch.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>

		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">商品検索</div>
        <hr>

        <div class="form">
            <form action="EditSerchResult" method="get">
                <fieldset>
                    <div>
                      <input id="word" name="keyword" type="text" />
                      <label for="word">キーワード</label>
                    </div>

                    <div>
                      <select id="category" name="category">
                        <option value="1">椅子</option>
                        <option value="2">ソファ</option>
                        <option value="3">テーブル・デスク</option>
                        <option value="4">ベッド</option>
                        <option value="5">収納</option>
                        <option value="6">ミラー</option>
                        <option value="7">雑貨</option>
                      </select>
                      <label for="category">カテゴリ</label>
                    </div>

                    <div>
                      <select id="sort" name="sort">
                        <option value="1">価格(高い順)</option>
                        <option value="2">価格(低い順)</option>
                        <option value="3">新しい順</option>
                      </select>
                      <label for="sort">並びかえ</label>
                    </div>

                    <div class="button">
                        <p><input type="submit" value="検索する"></p>
                    </div>

                </fieldset>

            </form>
        </div>


		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
