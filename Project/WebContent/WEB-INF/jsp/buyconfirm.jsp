<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>購入確認 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/buyconfirm.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">購入確認</div>
        <hr>

            <div class="item-list">
                <table>
                    <tr>
                        <th>商品</th>
                        <th>価格</th>
                    </tr>
                    <c:forEach var="item" items="${cart}">
	                    <tr>
	                        <td>${item.name}</td>
	                        <td>¥${item.formatPrice}</td>
	                    </tr>
	                </c:forEach>
                </table>
            </div>

            <div class="sum">
                <table>
                    <tr>
                        <th>小計</th>
                        <td>¥${bdb.formatSubTotalPrice}</td>
                    </tr>
                    <tr>
                        <th>送料</th>
                        <td>¥${bdb.formatDeliveryMethodPrice}</td>
                    </tr>
                    <tr>
                        <th>合計</th>
                        <td>¥${bdb.formatTotalPrice}</td>
                    </tr>
                </table>
            </div>

			<form action="BuyResult" method="post">
	            <div class="button">
	                <p><input type="submit" value="購入を確定する" class="button"></p>
	            </div>
            </form>
		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
