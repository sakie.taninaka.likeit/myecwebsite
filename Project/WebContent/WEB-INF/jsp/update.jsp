<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>お客様情報変更 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/register.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">お客様情報変更</div>
        <hr>

        <div class="red-text">
	        <c:if test="${validationMessage != null}">
				<P>${validationMessage}</P>
			</c:if>
		</div>

        <div class="form">
            <form action="UpdateConfirm" method="post">
                <fieldset>
                    <div>
                      <input id="name" name="name" type="text" value="${udb.name}" required class="input" />
                      <label for="name">お名前 :</label>
                    </div>
                    <div>
                      <input id="mail" name="mail" type="email" value="${udb.mail}" placeholder="necoaci@example.com" required class="input" />
                      <label for="mail">メールアドレス :</label>
                    </div>

                    <div>
                      <input id="mail" name="mail2" type="email" value="${udb.mail}" placeholder="necoaci@example.com" required class="input" />
                      <label for="name">メールアドレス(確認用) :</label>
                    </div>

                    <div>
                      <input type="tel" name="phone" id="phone" value="${udb.phone}" required class="input"/>
                      <label for="phone">電話番号 :</label>
                    </div>

                    <div>
                      <input type="text" name="address" id="address" value="${udb.address}" required class="input"/>
                      <label for="address">住所 :</label>
                    </div>

                    <div>
                      <input type="password" name="pw" id="pw" required class="input"/>
                      <label for="pw">パスワード :</label>
                    </div>

                    <div>
                      <input type="password" name="pw2" id="pw" required class="input" />
                      <label for="pw">パスワード(確認用) :</label>
                    </div>

                    <div class="button">
                        <p><input type="submit" value="更新する"></p>
                    </div>

                </fieldset>

            </form>
        </div>




		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
