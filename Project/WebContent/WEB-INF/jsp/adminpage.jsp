<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>Admin Page | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/mypage.css" rel="stylesheet">

</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>



	<!--wrap-->
	<div id="wrap">
        <div class="content">
            <div class="title">管理者ページ</div>
            <hr>

             <ul>
                <li><a href="ContactList">お問い合わせ一覧</a></li>
            </ul>
            <ul>
                <li><a href="EditSerch">販売商品の編集</a></li>
            </ul>
            <ul>
                <li><a href="ItemPlus">販売商品の追加</a></li>
            </ul>

            <form action="Logout" method="get">
	            <div class="button">
	                <p><input type="submit" value="ログアウト"></p>
	            </div>
            </form>

	   </div>
	</div>

	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
