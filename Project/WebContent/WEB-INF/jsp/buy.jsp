<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>購入 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/buy.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">購入する</div>
        <hr>
        	<h4>${buyActionMessage}</h4>

        	<c:forEach var="item" items="${cart}">
            	<div class="buy">
	                <div class="img-area">
	                    <div class="item-img">
	                        <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}" width="150px"></a>
	                    </div>
	                </div>

	                <div class="item">
	                    <table class="table">
	                        <tr>
	                            <td width="300px"><p>${item.name}</p></td>
	                            <td>¥${item.formatPrice}</td>
	                            <td class="delete">
	                            	<form action="BuyDelete" method="POST">
		                            	<input type="hidden" name="delete_item_id" value="${item.id}">
		                                <input type="submit" value="商品を削除する &#xf00d;" class="fas">
	                            	</form>
	                            </td>
	                        </tr>
	                    </table>
	                </div>
            	</div>
            </c:forEach>

			<form action="BuyConfirm" method="POST">
	            <div class="button">
	                <p><input type="submit" value="購入確認" class="button"></p>
	            </div>
	        </form>



		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
