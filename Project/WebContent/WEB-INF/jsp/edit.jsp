<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
<title>商品情報の編集 | necoaci</title>

<jsp:include page="/baselayout/head.html" />

<link href="css/edit.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>

	<!--wrap-->
	<div id="wrap">
		<div class="content">
			<form action="EditResult" method="post">
				<div class="item">
					<img src="img/${editItem.fileName}">



					<div class="item-inf">


						<div class="name">
							<label for="item-name">商品名</label> <input type="text"
								name="item-name" value="${editItem.name}">
						</div>
						<div class="price">
							<label for="item-price">価格</label> <input type="text"
								name="item-price" value="${editItem.price}">
						</div>
						<div class="inf">
							<label for="text">商品説明</label>
							<textarea name="text">${editItem.detail}</textarea>
						</div>




						<div class="delete">
							<a href="ItemDelete?id=${editItem.id}" class="fas">&#xf00d
								商品を削除する</a>
						</div>
					</div>


				</div>

				<ul class="button">
					<li><a href="javascript:history.back()">
							<div class="button-left">
								<input type="submit" value="戻る" class="button-white">
							</div>
					</a></li>
					<li>
						<div class="button-right">
							<input type="submit" value="更新する" class="button-black">
						</div>
					</li>
				</ul>
			</form>


		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"
					width="200"></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i
						class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
