<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>Log In | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/login.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


	<!--wrap-->
	<div id="wrap">


        <div class="content">


        <div class="form-area">

            <div class="title">ログイン</div>
        <hr>

        <c:if test="${loginErrorMessage != null}">
				<P class="red-text">${loginErrorMessage}</P>
			</c:if>

            <div class="login-left">
                    <form action="LoginResult" method="post">
                        <p>
                            <label for="mail">ログインID(メールアドレス)</label><br>
                            <input type="email" name="mail" id="mail" placeholder="necoaci@example.com" required>
                        </p>
                        <p>
                            <label for="pw">パスワード</label><br>
                            <input type="password" name="pw" id="pw" required>
                        </p>
                        <div class="button">
                            <input type="submit"  value="ログイン">
                        </div>
                    </form>
            </div>

            <div class="login-right">
                <form action="Regist" method="get">
                    <p>新規会員登録されるお客様はこちら</p>
                    <div class="button">
                        <input type="submit"  value="新規会員登録へ">
                    </div>
                </form>
            </div>




		</div>
	</div>

    </div>

	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
