<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>輸入家具店　necoaci</title>

	<jsp:include page="/baselayout/head.html" />

	<link href="css/index.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


	<!--wrap-->
	<div id="wrap">
		<div class="content">
			<h2>NEW ARRIVALS</h2>

			<!-- Swiper -->
  			<div class="swiper-container">
				<div class="swiper-wrapper">
					<c:forEach var="item" items="${newItemList}">
						<div class="swiper-slide"><a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a></div>
					</c:forEach>
				</div>
				<div class="swiper-button-prev swiper-button-black" ></div>
				<div class="swiper-button-next swiper-button-black"></div>
  			</div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>


  <!-- Swiper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>


  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
		navigation: {
    	nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
		},
  		loop: true,
	});
  </script>

            <div class="favorite">
				<h2>お気に入りアイテム</h2>

				<div class="red-text">
			        <c:if test="${validationMessage2 != null}">
						<P>${validationMessage2}</P>
					</c:if>
				</div>

                <ul class="favorite-list">
	                <c:forEach var="item" items="${favoriteItemList}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
	                 </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                        </div>
                        <div class="item-price">
                            <p><a href="Favorite">もっと見る</a></p>
                        </div>
                     </li>

                </ul>

			</div>

		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
