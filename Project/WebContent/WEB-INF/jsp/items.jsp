<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>ITEMS | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/items.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


	<!--wrap-->
	<div id="wrap">
        <div class="content">
            <div class="title">商品一覧</div>
                <hr>

                <h4>椅子</h4>
                <div class="chair-list">

                <ul>
	                <c:forEach var="item" items="${itemList1}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <form action="Allitems" method="get">
	                        <div class="item-price">
	                            <a href="AllItems?category=1"><p>もっと見る</p></a>
	                        </div>
                        </form>
                     </li>
                </ul>

                </div>

                <h4>ソファ</h4>
                <div class="sofa-list">

                <ul>
                    <c:forEach var="item" items="${itemList2}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=2"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>

                <h4>テーブル・デスク</h4>
                <div class="table-list">

                <ul>
                    <c:forEach var="item" items="${itemList3}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=3"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>

                <h4>ベッド</h4>
                <div class="bed-list">

                <ul>
                    <c:forEach var="item" items="${itemList4}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=4"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>

                <h4>収納</h4>
                <div class="closet-list">

                <ul>
                    <c:forEach var="item" items="${itemList5}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=5"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>
                <h4>ミラー</h4>
                <div class="mirror-list">

                <ul>
                    <c:forEach var="item" items="${itemList6}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=6"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>
                <h4>雑貨</h4>
                <div class="goods-list">

                <ul>
                    <c:forEach var="item" items="${itemList7}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                    </li>
                    </c:forEach>

                    <li>
                        <div class="item-photo">
                        </div>
                        <div class="item-name">
                            <a href="./allitems.html"><p>・・・</p></a>
                        </div>
                        <div class="item-price">
                            <a href="AllItems?category=7"><p>もっと見る</p></a>
                        </div>
                     </li>
                </ul>

                </div>







		</div>
	</div>



	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
