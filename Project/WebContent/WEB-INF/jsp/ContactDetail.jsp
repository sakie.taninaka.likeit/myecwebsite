<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>お問い合わせ詳細 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/buyhistorydetail.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>



		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">お問い合わせ詳細</div>
        <hr>
            <div class="history">
                 <table>
                    <tr>
                        <th>お名前</th>
                        <th>メールアドレス</th>
                        <th>電話番号</th>
                    </tr>
	                <tr>
	                     <td>${CDB.name}</td>
	                     <td>${CDB.mail}</td>
	                     <td>${CDB.phone}</td>
	                 </tr>
                </table>

            </div>

            <div class="historydetail">
                <table>
                    <tr>
                        <th>お問い合わせ内容</th>
                    </tr>
                    <tr>
	                    <td>${CDB.message}</td>
	                </tr>
                </table>

            </div>
			<a href="ContactList">
	            <div class="button">
	                <p><input type="submit" value="お問い合わせ一覧へ" class="button"></p>
	            </div>
            </a>




		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
