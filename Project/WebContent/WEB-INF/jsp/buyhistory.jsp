<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>購入履歴 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/buyhistory.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">購入履歴</div>
        <hr>
            <div class="history">
                <table>
                    <tr>
                        <th></th>
                        <th>購入日時</th>
                        <th>配送料</th>
                        <th>お支払金額</th>
                    </tr>
                    <c:forEach var="bhb" items="${buyHistoryList}" >
	                    <tr>
	                        <td><a href="BuyHistoryDetail?buy_id=${bhb.buyId}"><input type="submit" value="&#xf13a;" class="fas"></a></td>
	                        <td>${bhb.formatDate}</td>
	                        <td>¥${bhb.formatDeliveryMethodPrice}</td>
	                        <td>¥${bhb.formatTotalPrice}</td>
	                    </tr>
                    </c:forEach>
                </table>

            </div>




		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
