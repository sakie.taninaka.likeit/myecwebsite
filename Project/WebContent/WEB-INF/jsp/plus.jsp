<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>商品の追加 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/plus.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">

			<form action="ItemPlusResult" method="post">
	            <div class="item">
	                <div class="img">
	                    <script type="text/javascript">
	                    $(function() {
	                      $('input[type=file]').after('<span></span>');

	                      // アップロードするファイルを選択
	                      $('input[type=file]').change(function() {
	                        var file = $(this).prop('files')[0];

	                        // 画像以外は処理を停止
	                        if (! file.type.match('image.*')) {
	                          // クリア
	                          $(this).val('');
	                          $('span').html('');
	                          return;
	                        }

	                        // 新幅・高さ
	                        var new_w = 400;
	                        var new_h = 400;


	                        // 画像表示
	                        var reader = new FileReader();
	                        reader.onload = function() {
	                          var img_src = $('<img>').attr('src', reader.result);

	                          var org_img = new Image();
	                          org_img.src = reader.result;
	                          org_img.onload = function() {
	                            // 元幅・高さ
	                            var org_w = this.width;
	                            var org_h = this.height;
	                            // 幅 ＜ 規定幅 && 高さ ＜ 規定高さ
	                            if (org_w < new_w && org_h < new_h) {
	                              // 幅・高さは変更しない
	                              new_w = org_w;
	                              new_h = org_h;
	                            } else {
	                              // 幅 ＞ 規定幅 || 高さ ＞ 規定高さ
	                              if (org_w > org_h) {
	                                // 幅 ＞ 高さ
	                                var percent_w = new_w / org_w;
	                                // 幅を規定幅、高さを計算
	                                new_h = Math.ceil(org_h * percent_w);
	                              } else if (org_w < org_h) {
	                                // 幅 ＜高さ
	                                var percent_h = new_h / org_h;
	                                // 高さを規定幅、幅を計算
	                                new_w = Math.ceil(org_w * percent_h);
	                              }
	                            }

	                            // リサイズ画像
	                            $('span').html($('<canvas>').attr({
	                              'id': 'canvas',
	                              'width': new_w,
	                              'height': new_h
	                            }));
	                            var ctx = $('#canvas')[0].getContext('2d');
	                            var resize_img = new Image();
	                            resize_img.src = reader.result;
	                            ctx.drawImage(resize_img, 0, 0, new_w, new_h);
	                          };
	                        }
	                        reader.readAsDataURL(file);
	                      });
	                    });
	                </script>
	                    <input type="file" name="item-file" accept="image/*" class="img-box" required class="input">
	                </div>

	                <div class="item-inf">
	                    <div class="category">
	                        <label for="item-name">カテゴリ</label>
	                        <select id="category" name="category">
	                            <option value="1">椅子</option>
	                            <option value="2">ソファ</option>
	                            <option value="3">テーブル・デスク</option>
	                            <option value="4">ベッド</option>
	                            <option value="5">収納</option>
	                            <option value="6">ミラー</option>
	                            <option value="7">雑貨</option>
	                        </select>
	                    </div>
	                    <div class="name">
	                        <label for="item-name">商品名</label>
	                        <input type="text" name="item-name" required class="input">
	                    </div>
	                    <div class="price">
	                        <label for="item-price">価格</label>
	                        <input type="text" name="item-price" required class="input">
	                    </div>
	                    <div class="inf">
	                        <label for="item-detail">商品説明</label>
	                        <textarea name="item-detail" required class="input"></textarea>

	                    </div>
	                </div>
	            </div>

				<div class="button">
	                    <input type="submit"  value="商品を追加する" class="button-black">
	            </div>
	    	</form>
		</div>

	</div>



	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
