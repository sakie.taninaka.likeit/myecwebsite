<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>商品詳細 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/itempage.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">

            <div class="item">
                <img src="img/${item.fileName}"  >


                <div class="item-inf">
                    <div class="name">
                        <h3>${item.name}</h3>
                    </div>
                    <div class="price">
                        <p>${item.formatPrice}円</p>
                    </div>
                    <div class="inf">
                        <p>${item.detail}</p>
                    </div>

                    <form action="FavoriteAdd" method="POST">
                    	<input type="hidden" name="item_id" value="${item.id}">
	                    <div class="favorite">
	                        <input type="submit" value="&#xf004;" class="far"><p>商品をお気に入りに追加する</p>
	                    </div>
					</form>
                </div>
            </div>

            <ul class="button">
                <li>
                	<a href="javascript:history.back()">
	                    <div class="button-left">
	                        <input type="submit"  value="戻る" class="button-white">
	                    </div>
                    </a>
                </li>
                <li>
                	<form action="CartAdd" method="POST">
                		<input type="hidden" name="item_id" value="${item.id}">
	                    <div class="button-right">
	                        <input type="submit"  value="商品をカートに入れる" class="button-black">
	                    </div>
                    </form>
                </li>
            </ul>



		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
