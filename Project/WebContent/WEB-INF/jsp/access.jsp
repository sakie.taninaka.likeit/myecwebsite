<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>

	<title>ACCESS | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/access.css" rel="stylesheet">

</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>

	<!--wrap-->
	<div id="wrap">
        <div class="content">
                <div class="title">店舗情報</div>
                    <hr>
                    <div class="img">
                        <img src="img/shop.jpg" alt="necoaci"  width="600px">
                    </div>

                    <table>
                        <tr>
                            <th>所在地</th>
                            <td>〒123-4567<br>東京都中央区日本橋◎◎◎-21</td>
                        </tr>
                        <tr>
                            <th>TEL</th>
                            <td>012-3456-7890</td>
                        </tr>
                        <tr>
                            <th>電車でのアクセス</th>
                            <td>「水天宮前駅」より徒歩5分</td>
                        </tr>
                    </table>
                <section>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.756618408785!2d139.782964314577!3d35.682994337483954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018894450d8d7a9%3A0x92f0f935c94b5461!2z5rC05aSp5a6u5YmN6aeF!5e0!3m2!1sja!2sjp!4v1582376129819!5m2!1sja!2sjp" width="460" height="220" frameborder="0" style="border:0;"></iframe>
                </section>
            </div>




		</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
