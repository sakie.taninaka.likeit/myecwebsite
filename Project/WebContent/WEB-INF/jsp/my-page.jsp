<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>My Page | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/mypage.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>

	<!--wrap-->
	<div id="wrap">
        <div class="content">
            <div class="title">マイページ</div>
            <hr>

            <ul>
                <li><a href="BuyHistory">ご注文履歴</a></li>
            </ul>
            <ul>
                <li><a href="Update">お客様情報の更新</a></li>
            </ul>
            <ul>
                <li><a href="Favorite">お気に入りリスト</a></li>
            </ul>
			<form action="Logout" method="get">
	            <div class="button">
	                <p><input type="submit" value="ログアウト"></p>
	            </div>
            </form>

	   </div>
	</div>

	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
