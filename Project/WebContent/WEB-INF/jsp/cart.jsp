<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html>

<head>
	<title>カート | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/cart.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">カート内の商品</div>
        <hr>
            <h4>${cartActionMessage}</h4>

            <div class="cart-list">
                <ul>
	                <c:forEach var="item" items="${cart}">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}"><img src="img/${item.fileName}" ></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
	                            <p>¥${item.formatPrice}</p>
	                        </div>
	                        <form action="CartDelete" method="POST">
		                        <div class="delete">
		                        	<input type="hidden" name="delete_item_id" value="${item.id}">
		                            <input type="submit" value="&#xf00d;" class="fas" >
		                        </div>
		                    </form>
	                    </li>
	                </c:forEach>
				</ul>

				 </div>

			<a href="Items">
	            <div class="button-left">
	                <input type="submit"  value="ショッピングを続ける" class="button-white">
	            </div>
	        </a>

			<form action="Buy" method="get">
	            <div class="button-right">
	                <input type="submit"  value="レジへ"  class="button-black">
	            </div>
	        </form>



		</div>
	</div>



	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
