<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>購入完了 | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/buyresult.css" rel="stylesheet">
</head>

<body>

	<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
            <h4>購入が完了しました</h4>
            <div class="button">

				<form action="MyPage" method="get">
	                <div class="button-left">
	                   <input type="submit"  value="マイページへ" class="button-white">
	                </div>
                </form>
                <form action="Items" method="get">
	                <div class="button-right">
		                    <input type="submit"  value="ショッピングを続ける"  class="button-black">
		                </div>
	            </form>
            </div>


		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
