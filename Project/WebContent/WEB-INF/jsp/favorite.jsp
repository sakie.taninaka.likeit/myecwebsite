<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>お気に入り | necoaci</title>

	<jsp:include page="/baselayout/head.html" />

    <link href="css/favorite.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


	<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">お気に入りリスト</div>
            <hr>
            <div class="red-text">
		        <c:if test="${validationMessage != null}">
					<P>${validationMessage}</P>
				</c:if>
			</div>
			<div class="red-text">
			        <c:if test="${validationMessage2 != null}">
						<P>${validationMessage2}</P>
					</c:if>
				</div>

			<p>全${itemCount}件</p>

            <div class="favorite-list">

                <ul>
                	<c:forEach var="item" items="${itemList}" varStatus="status">
	                    <li>
	                        <div class="item-photo">
	                            <a href="Item?item_id=${item.id}&page_num=${pageNum}"><img src="img/${item.fileName}"></a>
	                        </div>
	                        <div class="item-name">
	                            <p>${item.name}</p>
	                        </div>
	                        <div class="item-price">
		                            <p>¥${item.formatPrice}</p>
		                    </div>
		                    <form action="Favorite" method="POST">
		                        <div class="delete">
		                        	<input type="hidden" name="delete_item_id" value="${item.id}">
		                            <input type="submit" value="&#xf00d;" class="fas">
		                        </div>
	                        </form>
	                    </li>
                    </c:forEach>
                </ul>


                </div>

                <div class="pagination">
                    <ul>
                    	<!-- １ページ戻るボタン  -->
						<c:choose>
							<c:when test="${pageNum == 1}">
		                        <li class="pre">＜</li>
		                    </c:when>
		                    <c:otherwise>
								<li class="pre"><a href="Favorite?inputMail=${inputUserMail}&page_num=${pageNum - 1}">＜</a></li>
							</c:otherwise>
						</c:choose>

						<!-- ページインデックス -->
						<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
							<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="Favorite?inputMail=${inputUserMail}&page_num=${status.index}">${status.index}</a></li>
						</c:forEach>

						<!-- 1ページ送るボタン -->
						<c:choose>
						<c:when test="${pageNum == pageMax || pageMax == 0}">
							<li class="next">＞</li>
						</c:when>
						<c:otherwise>
							<li class="next"><a href="Favorite?inputMail=${inputUserMail}&page_num=${pageNum + 1}">＞</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
                </div>

        </div>



	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
