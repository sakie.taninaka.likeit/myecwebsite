<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>

<head>
	<title>CONTACT | necoaci</title>
	<jsp:include page="/baselayout/head.html" />
    <link href="css/contact.css" rel="stylesheet">
</head>

<body>

<!--header-->
	<header>
		<jsp:include page="/baselayout/header.jsp" />
	</header>


		<!--wrap-->
	<div id="wrap">
        <div class="content">
        <div class="title">お問い合わせ</div>
        <hr>

            <div class="red-text">
		        <c:if test="${validationMessage != null}">
					<P>${validationMessage}</P>
				</c:if>
			</div>

        <div class="form">
            <form action="ContactConfirm" method="post">
                <p>
                    <label for="name" class="label">お名前</label><br>
                    <input type="text" name="name" id="name" autofocus required>
                </p>
                <p>
                    <label for="mail">メールアドレス</label><br>
                    <input type="email" name="mail" id="mail" placeholder="necoaci@example.com" autofocus required>
                </p>
                <p>
                    <label for="phone">電話番号</label><br>
                    <input type="tel" name="phone" id="phone" autofocus required>
                </p>
                <p>
                    <label for=message>お問い合わせ内容</label><br>
                    <textarea name="message" id="message" autofocus required></textarea>
                </p>
                <div class="button">
                    <p><input type="submit" value="送信"></p>
                </div>

            </form>
        </div>


		</div>
	</div>


	<!--footer-->
	<footer>
		<div class="main-ft">
			<div class="logo-ft">
				<a href="Index"><img src="img/logo_necoaci_wh.png" alt="necoaci"  width="200" ></a>
			</div>

			<ul class="global-nav-ft">
				<li><a href="About">ABOUT</a></li>
				<li><a href="Items">ITEMS</a></li>
				<li><a href="Access">ACCESS</a></li>
				<li><a href="Contact">CONTACT</a></li>
				<li><a href="https://www.instagram.com/◯◯◯◯◯"><i class="fab fa-instagram"></i></a></li>
			</ul>

		</div>

		<div class="sub-ft">
			<small>©2020 necoaci</small>
		</div>
	</footer>
</body>

</html>
